﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Interop;
using Thread = Java.Lang.Thread;

namespace App1
{
    [Activity(Label = "App1", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        public static volatile int alloced = 0;

        private static Task Allocator()
        {
            return Task.Run(() =>
            {
                for (;;)
                {
                    while(alloced > 1000) Thread.Sleep(10);
                    new Obj();
                    Interlocked.Increment(ref alloced);
                }
            });
        }

        private Task X0 = Allocator();
        private Task X1 = Allocator();
        private Task X2 = Allocator();
        private Task X3 = Allocator();
        private Button _Button;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            _Button = FindViewById<Button>(Resource.Id.MyButton);

            _Button.Click += delegate { _Button.Text = string.Format("{0} clicks!", count++); };
            Task.Run(() =>
            {
                int c = 0;
                int tot = 0;
                for (; ; )
                {
                    Thread.Sleep(10);
                    var f = alloced;
                    Console.WriteLine("Will gc {0}, {1}, {2}", ++c, f, tot += f);
                    GC.Collect();
                    Interlocked.Add(ref alloced, -f);
                    RunOnUiThread(() =>
                    {
                        _Button.Text = string.Format("grefs {0}", Runtime.GlobalReferenceCount);
                    });
                }
            });
        }

        public class Obj : Java.Lang.Object
        {
            
        }
    }
}

